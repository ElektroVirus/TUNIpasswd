# TUNIpasswd

This program is used to renew password expiration time (9 months) at Tampere
University (TUNI). The password can not be directly used again but after
changing the password 10 times, the original password is accepted again. So, I
automated it.

## Why?

Changing the password every 9 months does not only mean I have to change the
password for my intra account but also, that I have to change the password from
2 devices Wi-Fi and 2 devices Thunderbird email client. Imagine the amount of
frustration that I used a weekend developing this program.

Forcing password change like this only leads to users choosing poor passwords
like appending '1' to it every time or using the password before the current
one (nowadays the password can't be anymore used before ~10 other password
changes).

These poorly chosen passwords will be more predictable to computers and more
difficult for humans to remember. It would be much better to teach people to
use password managers and completely random passwords, but it is not possible
in our university since the password must be remembered e.g. in an electronic
exam.

## Installation

```
# Clone this repo
git clone https://gitlab.com/ElektroVirus/TUNIpasswd.git
cd TUNIpasswd

# Create a virtual environment
python -m venv venv
. venv/bin/activate

# Install requirements
pip install -r requirements.txt
```

## Usage

First you need to give this program your intra account loginname and password.
They are placed in *cfg/auth.ini*:

```
[credentials]
username = tanja.teekkari@tuni.fi
password = 9laillinenTuniSana
```

Easiest way to run this program. This will change the password to 15 random
passwords after which it will again change the password to the one given in
*cfg/auth.ini*:
```
python src/main.py
```

Change the password to something different:
```
python src/main.py -N 8uusiTuniSana
```

Don't want to write anything to configuration file? Everything can be given as
CLI arguments: (note that the new password will be written in *cfg/auth.ini*
and *cfg/passwords.txt*)
```
python src/main.py -U tanja.teekkari@tuni.fi -P 9laillinenTuniSana -N 8uusiTuniSana
```

## Help! This #@!%& program ate my password and I'm not able to log in anymore!

Firsly, I am sorry. Please, open up an issue and decribe what you did most
exactly so I can try to fix it.  

This program will output every password it creates so in case a bug, you can
try those out. The passwords will also be written to *cfg/passwords.txt*.

You can change the TUNI password at
[password.tuni.fi](https://password.tuni.fi/frontpage) by authenticating with
bank credentials etc. like you would do, if you forgot your password.

## Further development

CLI needs improvement. This program could be useful for only changing the
password once, which does not need purging the password history by changing the
password 15 times. 
