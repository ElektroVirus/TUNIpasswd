import requests

class FirefoxMockupSession(requests.Session):
    """
        This class inherits a requests session, which is crafted using special request headers from 
        Firefox. I don't know if it is needed, at least it is a little  bit less suspicious than
        if in the request the header tells the server that "Python Requests" is connecting.
    """

    def __init__(self):
        super().__init__()
        self._fake_firefox_headers()

    def _fake_firefox_headers(self):
        # Copied headers from Firefox running on Fedora 33
        headers = {
                "User-Agent":   "Mozilla/5.0 (X11; Linux x86_64; rv:93.0) Gecko/20100101 Firefox/93.0",
                "Accept":       "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                "Accept-Language":              "en-US,en;q=0.5",
                "Accept-Encoding":              "gzip, deflate, br",
                "DNT":                          "1",
                "Connection":                   "keep-alive",
                "Upgrade-Insecure-Requests":    "1",
                "Sec-GPC":                      "1",
                "Cache-Control":                "max-age=0"
        }
        self.headers.update(headers)


