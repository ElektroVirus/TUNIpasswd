from firefoxmockupsession import FirefoxMockupSession
from urlbuilder import URLBuilder
from bs4 import BeautifulSoup
from datetime import datetime
from typing import Tuple

class PasswordChangeException(Exception):
    """
    :brief: Exception which is thrown on password changing
    """
    pass

class LoginException(Exception):
    """
    :brief: Exception which is thrown on login attempt
    """
    pass

class LogoutException(Exception):
    """
    :brief: Exception which is thrown on logout attempt
    """
    pass

class AcceptTermsException(Exception):
    """
    :brief: Exception which is thrown on accepting the terms and conditions of password.tuni.fi
    """
    pass


class TUNIpasswdSessionManager():
    
    def __init__(self, urlbuilder: URLBuilder):
        """
        :brief: TUNIpasswdSessionManager is a manager for requests session logged in 
                password.tuni.fi. It is the portal where the TUNI password can be changed.
                Unlike other TUNI services, this is one of the portals which don't need MFA 
                (EXAM electronic exam is the second one I know)
        :param urlbuilder: Instance to URLBuilder singleton class which provides the neccessary
                URLs
        """
        self.__urlgen = urlbuilder
        self.__session = FirefoxMockupSession()
        self.__session.cookies.clear()


    def login(self, loginname: str, password: str, csrf_token_name: str, login_token_name) \
            -> Tuple[str, str, datetime]:
        """
        :brief: Logs into portal
        :param loginname:   TUNI username, ends with '@tuni.fi'
        :param password:    Password
        :param csrf_token_name: Cross Site Request Forgery token name which is embedded into HTML 
                                of portal.tuni.fi
        :login_token_name:  Login session cookie name
        :return:            tuple(login_token, csrf_token, login datetime)
        :throws:            LoginException
        """
                    
        url = self.__urlgen.frontpage()
        frontpage_get = self.__session.get(url)

        # For debugging purposes
        # with open('html/frontpage_get.html','w') as fd:
        #     fd.write(frontpage_get.text)

        # Get CSRF-tag. Unique for each visit on site.
        soup = BeautifulSoup(frontpage_get.content, features="html.parser")
        csrf_token_value = soup.find("input", {"name": csrf_token_name})["value"]

        request_data = {
                "type":             "change",
                csrf_token_name:    csrf_token_value,
                "username":	    loginname,
                "password":	    password,
        }

        # Login to website
        login_post = self.__session.post(url, data=request_data)

        # For debugging purposes
        # with open('html/login_post.html','w') as fd:
        #     fd.write(login_post.text)

        # Search for login error. If div is found (is not None), error occurred. 
        # If None, no errors.
        soup = BeautifulSoup(login_post.content, features="html.parser")
        warning_div = soup.find("div", {"id": "loginFail"})

        if warning_div is not None:
            raise LoginException(f"Failed to login, probably wrong password for user '{loginname}'")
        if login_post.status_code != 200:
            raise LoginException(f"Failed to login, http status code {login_post.status_code}")

        # Delete CSRF-token since it is probably not anymore needed
        if csrf_token_name in self.__session.headers.keys():
            del self.__session.headers[csrf_token_name] 

        # Return tokens for future use 
        login_token = self.__session.cookies[login_token_name]
        initiated_dt = str(datetime.now())

        return (login_token, csrf_token_value, initiated_dt)


    def logout(self) -> None:
        """ 
        :brief: Logs session out of password.tuni.fi
        """
        request_data = {}
        url = self.__urlgen.logout()

        logout_get = self.__session.get(url, data=request_data)
        if logout_get.status_code != 200:
            raise LogoutException(f"Failed to logout, http status code {logout_get.status_code}")

        # For debugging purposes
        # with open('html/logout_get.html','w') as fd:
        #     fd.write(logout_get.text)

        self.__session.cookies.clear()
        return None


    def change_password(self, new_password: str, csrf_token_name: str, csrf_token: str) -> str:
        """
        :brief:     Change password. This request needs a new csrf-token, which can be obtained 
                    by accepting the terms of the page. Use function 'accept' for it.
                    Password change should be successful if the page returns 200.
        :param new_password:    New desired password. Must be TUNI-compliant. Note! This function 
                                does not check whether password change was successful! 
        :param csrf_token_name: Cross Site Request Forgery token name which is embedded into HTML 
                                of portal.tuni.fi accept terms -page
        :csrf_token: Cross Site Request Forgery token. Note, this is different token than the one
                        used for login.
        :return str New password
        :raise PasswordChangeException
        """

        ################################################################################ 
        # Warning! This function can't check whether password change was successful.   #
        # It is way more complicated than checking the return code of the request.     #
        ################################################################################ 

        request_data = {
                "password1": new_password, 
                "password2": new_password,
                csrf_token_name: csrf_token
        }

        url = self.__urlgen.password()

        change_passwd_post = self.__session.post(url, data=request_data)

        # For debugging purposes
        # with open('html/change_passwd_post.html','w') as fd:
        #     fd.write(change_passwd_post .text)

        if change_passwd_post.status_code != 200:
            raise PasswordChangeException(f"Failed to change password, http status code " + \
                    "{change_passwd_post.status_code}")

        return new_password


    def accept(self, csrf_token_name) -> str:
        """ 
            :brief: Accept terms and conditions. This page returns a new CSRF-token! 
            :param csrf_token_name: Name of Cross Site Request Forgery token. This function will
                                    fetch the second such token for salasana.tuni.fi.
            :return: CSRF-token for password change
            :raise AcceptTermsException
        """
        request_data = {}
        url = self.__urlgen.accept()

        accept_get = self.__session.get(url, data=request_data)
        if accept_get.status_code != 200:
            raise AcceptTermsException(f"Failed to accept terms, http status code {accept_get.status_code}")

        # Get CSRF-tag. Unique for each visit on site.
        soup = BeautifulSoup(accept_get.content, features="html.parser")
        csrf_token_value = soup.find("input", {"name": csrf_token_name})["value"]

        # For debugging purposes
        # with open('html/accept_get.html','w') as fd:
        #     fd.write(accept_get.text)

        return csrf_token_value


def main():
    from urlbuilder import URLBuilder
    urlgen = URLBuilder()
    tsm = TUNIpasswdSessionManager(urlgen)
    username = "teemu.teekkari@tuni.fi"
    password = "vaarasalasana"
    csrf_token_name = "_csrf"
    login_token_name = "JSESSIONID"
    credentials = tsm.login(username, password, csrf_token_name, login_token_name)


if __name__ == "__main__":
    main()
