import string
import secrets
import sys

ACCEPTED_SPECIAL_CHARS      = '+-*.:;#!_=()[]{}<>"'
ACCEPTED_LOWERCASE_LETTERS  = string.ascii_lowercase 
ACCEPTED_UPPERCASE_LETTERS  = string.ascii_uppercase
ACCEPTED_DIGITS             = string.digits

CHARS = ACCEPTED_SPECIAL_CHARS + \
        ACCEPTED_LOWERCASE_LETTERS + \
        ACCEPTED_UPPERCASE_LETTERS + \
        ACCEPTED_DIGITS
    

def get_random_string(length: int) -> str:
    """
    :brief: Returns a random string by randomly choosing characters from CHARS variable,
            which holds the alphabet TUNI accepts as password. 
    :length: int length of the desired password
    :return: str 
    """
    result = ''.join(secrets.choice(CHARS) for i in range(length))
    return result


def check_retarded_tuni_rules(new_password) -> bool:
    """
        :brief: Checks if password is acceptable by tuni rules.
        :return: True if ok, False if it needs to be generated again

        Note! This may be insufficient. The rules say also, that the password may not contain
        parts of username or real life words like 'password', 'salasana' or 'qwerty'. 
        Who knows how long the list is, so it's impossible to test.

    """

    """ Full password specification:
    - Comprises 10-40 characters
    - Includes lower and upper case characters
    - Includes at least 1 lower case character
    - Includes at least 1 upper case character
    - Includes at least 1 number or special character
    - Password don't support ���-letters
    - Must not include forbidden special characters
    - The allowed special characters are: + - * . : ; # ! _ = ( ) [ ] { } < > "
    - Must not be similar to your name or user name
    - Must not contain common words (e.q. password or qwerty)
    """

    # TODO: This does not check for common words like 'password' or whether password contains 
    # username.

    # Check that each character is accepted
    for c in new_password:
        if c not in CHARS:
            return False

    def contains_char_from_alphabet(word: str, alphabet: str) -> bool:
        """
            :brief Checks whether word contains at least one character from given alphabet.
            :param word: str 
            :param alphabet: str 
            :return True if word contains at least one occurance from alphabet, 
                    False otherwise
        """
        for c in alphabet:
            if c in word:
                return True
        return False

    # let's just check there is a character from each group (special characters, 
    # lower case and upper case characters.

    # Check length
    if len(new_password) < 10 or len(new_password) > 40: 
        return False

    # Password must include at least 1 lowercase letter
    if not contains_char_from_alphabet(new_password, ACCEPTED_LOWERCASE_LETTERS):
        return False

    # Password must include at least 1 uppercase letter
    if not contains_char_from_alphabet(new_password, ACCEPTED_UPPERCASE_LETTERS):
        return False

    # Password must include at least 1 number or special character (with De Morgan's Law)
    if not contains_char_from_alphabet(new_password, ACCEPTED_SPECIAL_CHARS) and \
            not contains_char_from_alphabet(new_password, ACCEPTED_DIGITS):
        return False

    # Congratulations! Password *should* TUNI-compliant.
    # Hopefully it does not contain your username or some simple words as 'password' or 'qwerty'
    return True


def xkcd_passphrase() -> str:
    """
    :brief: Returns a XKCD style password (copied from Python documentation), which is 4 random
        words catenated together.
        https://docs.python.org/3/library/secrets.html
        https://xkcd.com/936/
    :return: str
    """
    import secrets

    # On standard Linux systems, use a convenient dictionary file.
    # Other platforms may need to provide their own word-list.
    with open('/usr/share/dict/words') as f:
        words = [word.strip() for word in f]
        password = ' '.join(secrets.choice(words) for i in range(4))
    return password


def generate_random_tuni_password() -> str:
    """
    :brief: Generates random TUNI-compliant password with length 20 and checks (most) rules 
            that it really is compliant.
    :return: str
    """
    tmp_password = get_random_string(20)
    while not check_retarded_tuni_rules(tmp_password):
        tmp_password = get_random_string(20)
    return tmp_password


def main():
    print(generate_random_tuni_password())


if __name__ == "__main__":
    main()
