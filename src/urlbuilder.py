from urllib.parse import urljoin
import configparser
from datetime import datetime
from datetime import timedelta

class URLBuilderException(Exception):
    """
    :brief: Exception reserved for URLBuilder.
    """
    pass


class URLBuilderNotReadyException(Exception):
    """
    :brief: Exception reserved for URLBuilder if it is not constructed yet.
    """
    pass


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        """
        :brief: This is how singleton classes are created in Python. 
                The first time this class is called, it is instantiated and every other time the
                fist instance is returned. This is made by overriding the __call__ method.
        """
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class URLBuilder():
    """
    :brief: Class for building URLs for this program. The URLs are simple enough to simply
            be returned. This could be of type Singleton but at this time it is unnecessary.
    """

    def __init__(self):
        pass
        

    def frontpage(self) -> str:
        return "https://salasana.tuni.fi/frontpage"


    def logout(self) -> str:
        return "https://salasana.tuni.fi/logout"


    def password(self) -> str:
        return "https://salasana.tuni.fi/password"


    def accept(self) -> str:
        return "https://salasana.tuni.fi/accept"


    def check_password_status(self) -> str:
        return "https://salasana.tuni.fi/rs/check-password-status"
