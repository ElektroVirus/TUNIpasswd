from urlbuilder import URLBuilder
from tunipasswdsessionmanager import TUNIpasswdSessionManager
from tunipasswdsessionmanager import PasswordChangeException, LoginException
from tunipasswordgenerator import generate_random_tuni_password
from tunipasswordgenerator import check_retarded_tuni_rules

import configparser
import sys
import argparse

PASSWORD_RECOVERY_FILE = "cfg/passwords.txt"


def main():

    """
    Maybe a little too thicc main-function. 
    TODO: think about splitting it.
    """

    args = add_argparser_arguments()

    # Important variables
    username   = None
    password    = None
    new_password = None

    # Flags
    username_is_from_args = False

    if args.username is not None:
        username = args.username
        username_is_from_args = True 
    if args.old_password is not None:
        password = args.old_password
    if args.new_password is not None:
        if not check_retarded_tuni_rules(args.new_password):
            print("Warning! Password policy of TUNI probably won't accept this password. Aborting.")
            exit()
        else:
            new_password = args.new_password 

    if args.iterations is None:
        iterations = 15
    else:
        iterations = args.iterations

    config_auth  = configparser.ConfigParser()
    config_auth.read("cfg/auth.ini")

    config  = configparser.ConfigParser()
    config.read("cfg/config.ini")

    try:
        # If username was not given, read it from file.
        if username == None:
            username   = config_auth[f"credentials"]["username"]

        # It's unlikely that username is given from CLI argument but password read from file.
        # It's better to behave like there is no password.
        if password == None:
            if username_is_from_args:
                print("Username was given with -U, but password (-P) not. Please provide it. " + \
                        "Refusing to read cfg/auth.ini.")
                exit()
            else:
                password = config_auth["credentials"]["password"]
    except KeyError:
        print("Could not read authentication information from auth.ini.")
        exit()

    if new_password == None:
        new_password = password

    try:
        csrf_token_name = config["cookienames"]["csrf_token"]
        login_token_name = config["cookienames"]["login_token"]
    except KeyError:
        print("Could not read configuration information from config.ini.")
        exit()

    ################################### 
    # Purge password history          #
    ################################### 

    # The only mode this program works now in. It purges history and changes the password
    # to the desired one. 
    # TODO: This could be set behind a CLI argument, since e.g. if new password
    #   is completely new, this is not needed. 

    current_password = purge_password_history(username, password, csrf_token_name, \
            login_token_name, iterations)

    ###################################
    # Change password to new_password #
    ###################################
    try:
        change_password(username, current_password, new_password, csrf_token_name, \
                login_token_name)
    except PasswordChangeException:
        print("Unable to change password. Please try higher iteration (-n) for random passwords.")
        new_password = current_password


    config_auth["credentials"]["password"] = new_password

    # Write configuration file changes
    with open("cfg/auth.ini", "w") as configfile:
        config_auth.write(configfile)

    print("Success! Password changed: {}".format("*" * len(new_password)))


def add_argparser_arguments():
    """
    :brief: Creation of command line arguments for this program.
    """
    parser = argparse.ArgumentParser()

    help_new_pasword = "New desired password for password.tuni.fi or salasana.tuni.fi. " + \
            "If omitted, old password is used again."
    help_iterations = "In case resetting password expiration, the same password may not be " + \
            "used again. This program will set the password to a random word for n " + \
            "iterations until the old password may be used again. By the time writing of " + \
            "this n=15. This variable may be used to set a limit for max iterations."

    parser.add_argument("-U", "--username", type=str,
                    help="Username for password.tuni.fi or salasana.tuni.fi. If omitted, " +
                    "cfg/auth.ini is read.")
    parser.add_argument("-P", "--old-password", type=str,
                    help="Password for password.tuni.fi or salasana.tuni.fi. If omitted, " + 
                            "cfg/auth.ini is read.")
    parser.add_argument("-N", "--new-password", type=str,
                    help=help_new_pasword)
    parser.add_argument("-n", "--iterations", type=int, help=help_iterations)

    args = parser.parse_args()
    return args


def change_password(username: str, password: str, new_password: str, csrf_token_name: str, \
        login_token_name: str) -> None:
    """
        :brief: Automated password chaning.
        :param username:            Username for password.tuni.fi or salasana.tuni.fi. 
                                    Email-address, ends with @tuni.fi.
        :param password:            Password which is used to log in.
        :param new_password:        TUNI-compliant new password
        :param csrf_token_name:     The website has a Cross Site Request Forgery token embedded in 
                                    HTML.  It must be found manually from front page.
        :param login_token_name:    The site gives a login cookie, this is the name of it.

        :return: None
        :raises:    PasswordChangeException if password change failed (status_code != 200)
                    Note! This function will not (yet) detect password change failure! Be careful,
                    my friend.
    """

    ################################################################################ 
    # Warning! This function can't check whether password change was successful.   #
    # It is way more complicated than checking the return code of the request.     #
    ################################################################################ 

    urlgen = URLBuilder()
    tsm = TUNIpasswdSessionManager(urlgen)

    try:
        credentials = tsm.login(username, password, csrf_token_name, login_token_name)
    except LoginException:
        print(f"Couldn't log in. Probably wrong password for account '{username}'")
        exit()

    new_csrf_token = tsm.accept(csrf_token_name)
    tsm.change_password(new_password, csrf_token_name, new_csrf_token)

    with open(PASSWORD_RECOVERY_FILE, 'a') as fd:
        fd.write(new_password + '\n')

    tsm.logout()
    

def purge_password_history(username: str, password: str, csrf_token_name: str, \
        login_token_name: str, iterations: int) -> str:
    """
        :brief: Change the password multiple times to random passwords.
                This is because of TUNI's password policy where password may not be used again 
                before ~10 other passwords. So let's automate it.
                Note! After this function TUNI password will be completely random so 
                change_password is required to be called at least once more.
        :param username:            Username for password.tuni.fi or salasana.tuni.fi. 
                                    Email-address, ends with @tuni.fi.
        :param password:            Password which is used to log in.
        :param csrf_token_name:     The website has a Cross Site Request Forgery token embedded in 
                                    HTML.  It must be found manually from front page.
        :param login_token_name:    The site gives a login cookie, this is the name of it.
        :param iterations int:      How many random passwords to feed into system before trying 
                                    new_password.
        :return: str, last valid password
    """
    for i in range(iterations):
        tmp_password = generate_random_tuni_password()
        print(f"Using temporary password {tmp_password}")
        try:
            change_password(username, password, tmp_password, csrf_token_name, login_token_name)
        except PasswordChangeException:
            print("Unable to change password. Aborting.")
            return password
        else:
            password = tmp_password
    return password



if __name__ == "__main__":
    main()

